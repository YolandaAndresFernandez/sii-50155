#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <fcntl.h>

#define MAX_BUF 1024

int main (void)
{
   int t;
   char buffer[MAX_BUF];

   //Se crea la tuberia//

   mkfifo("/tmp/mipipe",0666); //los 6 dan permiso de lectura y escritura

   t=open("/tmp/mipipe",O_RDONLY); //abrir solo lectura
   
   if (t<0)
   {
     perror("ERROR AL ABRIR TUBERIA");
     exit(1);

   }
    
   //Lee el bucle desde la tuberia//

   while (1)
   {
     read(t,buffer, sizeof(buffer));

     //Imprime el valor que comparte el otro proceso//
     
     printf("%s\n",buffer); //perror("Recibido: %sn", buffer);

     if( strcmp(buffer,"Fin del programa")==0) break;
}
    close(t);
    unlink("/tmp/mipipe");
   
return 0;
}

