// Mundo.h: interface for the CMundo class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_

#include "DatosMemCompartida.h" 
#include <vector>
#include "Plano.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

//#include "Esfera.h"
//#include "Raqueta.h"

// PUNTO 1
#include <pthread.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <Socket.h> 

#define TAM 1024
#define TAMS 100

class CMundo  
{
public:
	
	int fifo;	//PUNTO 3 Tuberia logger

	//int fifo_s;
	//int fifo_hilo;
	
	int fichero;
	
	pthread_t thid1; //Identificador thread
	
	//Declaracion de Sockets	
	Socket sconex;
	Socket scomun;
	char nombre_s[15];
	char ip[1024];
	int sock;

	void Init();
	CMundo();
	virtual ~CMundo();	
	
	void InitGL();	
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();	
	
	void RecibeComandosJugador();
	

	Esfera esfera;
	std::vector<Plano> paredes;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;

	int puntos1;
	int puntos2;
	DatosMemCompartida datos; // PUNTO 1
	DatosMemCompartida *dat; //PUNTO 2
};

#endif // !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
